#!/bin/bash

date=$(date +'%Y-%m-%d')
hour=$(date +'%T')
year=$(date +'%Y')
month=$(date +'%m')
day=$(date +'%d')
processes=$(ps -eo comm,rss --sort -rss --no-headers | head -n 5 | numfmt --to=iec --from-unit=1024 --suffix=B --field 2 | awk '{OFS= ", "; print $1,$2}')

while read line; do
  mkdir -p ./$year/$month/$day
  echo $date, $hour, $line >> ./$year/$month/$day/$hour.log
done <<< $processes
