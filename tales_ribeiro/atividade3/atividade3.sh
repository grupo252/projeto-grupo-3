#!/bin/bash

number=$1

echo "Verificando se o número fornecido foi maior do que 2"
if [[ $number -gt 2 ]]; then
  for ((i = 1 ; i <= $number ; i++)); do
    echo "Inserindo as informações dos processos atuais no arquivo: iteracao-${i}.log"
    top -n 1 > iteracao-$i.log
    if [[ $(($i % 2)) -eq 0 ]]; then
      echo "Copiando iteracao-${i}.log para a pasta backup_par"
      mkdir -p backup_par
      cp iteracao-$i.log backup_par
    else
      echo "Copiando iteracao-${i}.log para a pasta backup_impar"
      mkdir -p backup_impar
      cp iteracao-$i.log backup_impar
    fi
  done
  echo "Juntando as pastas de backup em um arquivo tarball"
  tar -cvzf backup_par_impar.tar.gz backup_par backup_impar
else
  echo "Programa não executado (número menor que 2)"
fi
