#!/bin/bash

data='https://github.com/owid/covid-19-data/archive/refs/heads/master.zip'
filePath='./covid-19-data-master/public/data/'

#Baixando o repositório
wget $data

#Descompactando o repositório
unzip master.zip

cp $filePath'owid-covid-data.csv' $filePath'owid-covid-codebook.csv' .

mv owid-covid-data.csv dados-covid-owid.csv
mv owid-covid-codebook.csv manual-dados-covid-owid.csv

