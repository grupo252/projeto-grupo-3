#!/bin/bash

echo "Criando os arquivos log1.txt e log2.txt"
touch ./log1.txt ./log2.txt

echo "Colocando o output do comando ls -lha no log1.txt"
ls -lha > log1.txt

echo "Colocando o output do comando top no log2.txt"
top -n 1 > log2.txt

echo "Criando a pasta backup_logs"
mkdir -p ./backup_logs

echo "Copiando os arquivos log1.txt e log2.txt para a pasta backup_logs"
cp log1.txt log2.txt backup_logs

echo "Atividade concluída com sucesso"
