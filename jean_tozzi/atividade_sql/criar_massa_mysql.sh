#!/bin/bash

ARQUIVO_1=./nomes.txt
ARQUIVO_2=./datas.txt
QUANTIDADE_PROFS=$1
QUANTIDADE_REGISTROS=0

while IFS=$'\t' read -r f1 f2
do
  echo 'INSERT INTO FUNCIONARIO(NOME_FUNCIONARIO, DATA_ADMISSAO) VALUES("'$f1'", STR_TO_DATE("'$f2'", "%d/%m/%Y"));' >> ./sql/massa_mysql.sql
  ((QUANTIDADE_REGISTROS++))
done < <(paste $ARQUIVO_1 $ARQUIVO_2)

# Lista temporária de código de funcionário
LISTA_COD_FUN=($(shuf -i 1-$QUANTIDADE_REGISTROS -n $QUANTIDADE_PROFS | sort -n))
printf "%s\n" "${LISTA_COD_FUN[@]}" > temp1.txt

# Lista temporária de matrícula de professor
LISTA_MAT_PROF=($(shuf -i 1000-9999 -n $QUANTIDADE_PROFS | sort -n))
printf "%s\n" "${LISTA_MAT_PROF[@]}" > temp2.txt


while IFS=$'\t' read -r f1 f2
do
	TITULACAO=$(shuf -i 2-6 -n 1)
	echo 'INSERT INTO PROFESSOR(CODIGO_FUNCIONARIO, MATRICULA_PROFESSOR, CODIGO_TITULACAO) VALUES('$f1', '$f2', '$TITULACAO');' >> ./sql/massa_mysql.sql
done < <(paste ./temp1.txt ./temp2.txt)

rm -rf temp1.txt temp2.txt
