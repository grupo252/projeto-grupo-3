# Pipeline

![miro-frame.jpg](./miro-frame.jpg)

# Explicação
 - O docker-compose sobe 3 containers: **db**, **dumper** e **sender**.
 - **db**: é uma SGBD MariaDB que sobe já populado com o banco de dados FACULDADE (projeto desenvolvido em aula).
 - **dumper**: após 20s da execução do container **db**, o container **dumper** estabelece conexão como cliente MySQL com o container **db** e realiza o dump do banco de dados.
 - **sender**: após 10s da execução do dump, o container **sender** cria o Bucket AWS S3 com nome predefinido (caso já não tenha sido criado anteriormente) e salva numa pasta ANO-MES-DIA no Bucket. 

# Executando
    # criando as images dumper e sender:
    docker build -t dumper /images/dumper/
    docker build -t sender /images/sender/

    # subindo os containers
    docker-compose up -d
