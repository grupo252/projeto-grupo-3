#!/bin/bash

# atribuindo data e hora a variáveis
DATA=$(date +'%Y-%m-%d')
HORA=$(date +'%T')
HORA_TRACOS=$(date +'%H-%M-%S')

# gravando o resultado do ps formatado na variável
# explicações sobre os parâmetros:
# ps -eo comm,rss = mostra todos os procesos, selecionando somente comm,rss (nessa ordem)
# ps --sort -rss = ordena de acordo com a coluna rss DECRESCENTEMENTE (- na frente)
# ps --no-headers = não inclui o cabeçalho
# head -n 5 = pega somente as 5 primeiras linhas do output
# numfmt --to=iec --from-unit=1024 --suffix=B --field 2 = converte a coluna 2 (rss) de kB para MB (adicionando o sufixo B)
# awk {...; print $1, $2}' = separa os 2 outputs com espaço e vírgula
PS_FORMATADO=$(ps -eo comm,rss --sort -rss --no-headers | head -n 5 | numfmt --to=iec --from-unit=1024 --suffix=B --field 2 | awk ' {OFS=", "; print $1, $2 }')

# lê cada linha do output do ps (passado como input com o <<<) e concatena com data e hora, guardando num log.txt
while read line; do
	# estrutura do nome AAAA-MM-DD_hh-mm-ss do COMEÇO do script
	# >> é append (adiciona no final do doc)
	echo $DATA, $HORA, $line >> $DATA'_'$HORA_TRACOS.txt
done <<< $PS_FORMATADO
