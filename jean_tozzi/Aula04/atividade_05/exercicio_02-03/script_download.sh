#!/bin/bash

# links para download
DATA="https://github.com/owid/covid-19-data/raw/master/public/data/owid-covid-data.csv"
CODEBOOK="https://github.com/owid/covid-19-data/raw/master/public/data/owid-covid-codebook.csv"

# fazendo download e renomeando
wget -O "dados-covid-owid.csv" $DATA
wget -O "manual-dados-covid-owid.csv" $CODEBOOK

