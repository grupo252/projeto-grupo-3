#!/bin/bash

# local do arquivo
ARQUIVO="./dados-covid-owid.csv"

# retirando somente a coluna do nome do país (Col3), removendo as duplicadas e salvando em .txt
# explicação da sintaxe:
# awk -F , define o separador de campos como vírgula. print $3 retorna somente a 3a coluna do arquivo
# sort -u ordena alfabeticamente e remove valores duplicados
awk -F',' '{print $3}' $ARQUIVO | sort -u > paises-afetados-covid-owid.txt 
