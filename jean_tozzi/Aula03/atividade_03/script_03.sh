#!/bin/bash

# armazenando o argumento passado na chama
NUM=$1

# criando regex para SOMENTE NÚMEROS
REGEX="^[0-9]+$"

# checando se o argumento é um número
if [[ $NUM =~ $REGEX ]]; then
	# checando se o argumento é maior que 2
	if [[ $NUM -gt 2 ]]; then
		# criando as pastas previamente
		mkdir -p {backup_impar,backup_par}
		# estrutura de repetição que vai de 1 a NUM
		for ((i = 1; i <= $NUM; i++)); do
			top -n 1 > iteracao-$i.log
			# se par, copiar para pasta par
			if [[ $(($i % 2)) -eq 0 ]]; then
				cp iteracao-$i.log ./backup_par
			# se ímpar, copiar para pasta ímpar
			else
				cp iteracao-$i.log ./backup_impar
			fi
		done
		# juntando ambas as pastas num tarball
		tar -cf backup_par_impar.tar ./backup_impar ./backup_par
	else
		echo "Não será necessário executar o programa"
	fi
else
	echo "Insira um NÚMERO maior que 2 como argumento"
fi
