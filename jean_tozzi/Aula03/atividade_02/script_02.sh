#!/bin/bash

echo "Criando log1.txt e log2.txt..."
touch log1.txt log2.txt

echo "Preenchendo log1.txt com ls -lha..."
ls -lha > log1.txt

echo "Preenchendo log2.txt com top..."
# -n 1 para rodar apenas 1 iteração
top -n 1 > log2.txt

echo "Criando pasta backup_logs..."
# -p para não dar erro se a pasta já existir
mkdir -p backup_logs

echo "Copiando os logs para a pasta criada..."
cp log1.txt log2.txt backup_logs

echo "Atividade concluída com sucesso!"
