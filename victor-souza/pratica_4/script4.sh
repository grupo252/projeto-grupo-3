#!/bin/bash

# Definindo os padroes do formato de hora, nome do diretorio de armazenamento dos logs e nome do arquivo de log.
DATA_HORA_ATUAL=$(date +"%Y-%m-%d, %T")
DIRETORIO=$(date +"%Y_%m_%d")
LOG_NAME=$(date +"%Y_%m_%d-%H_%M")

# Extracao do nome do processo e tamanho de memoria alocada para o processo, ordenado de forma decrescente e ajustada a formatacao.
PROCESSOS=$(ps -eo comm,rss --no-headers --sort -rss | head -n 5 | numfmt --to iec --from-unit 1024 --suffix B --field 2 | awk '{OFS=", "; print $1,$2}')

# Concatenacao no diretorio e arquivo definido dos dados de data, hora, processo e tamanho de memoria.
mkdir -p $DIRETORIO
while read LINHA; do
	echo $DATA_HORA_ATUAL, $LINHA >> ./$DIRETORIO/$LOG_NAME.txt
done <<< $PROCESSOS
