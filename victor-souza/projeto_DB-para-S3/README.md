# Projeto Final

Este repositório demonstra alguns dos mecanismos necessários para carregar um arquivo em formato SQL em um banco de dados MariaDB rodando a partir de um container Docker, usar outro container com o MySQL-Client para realizar um dump remoto dos dados do primeiro container a partir de um script Shell/Bash, e um terceiro container para periodicamente (crontab) verificar a presença de um dump na pasta compartilhada com MySQL-Client e enviar este dump para um S3 Bucket na AWS usando um script Python.


## Executando o Exemplo

### Docker Images
Para este projeto, foi utilizada uma imagem do MariaDB pronta para uso e duas imagens customizadas para implementar o MySQL-Client e para implementar o trabalho Python orquestrado pelo Crontab.

1. MariaDB: usando a imagem MariaDB mais recente do DockerHub, a ideia nesse container é ingerir um arquivo SQL predefinido e continuar de pé em execução.

2. MySQL Dumper: foi criada uma imagem simples com MySQL-Client para poder logar no container MariaDB e extrair um dump no diretório /data/dumper/, ultilizando um arquivo shell script.

```sh
docker build -t client ./app/dumper/
```

3. Python com Crontab: esta imagem tem [python:3.8-alpine3.14](https://hub.docker.com/_/python), crontab e pacote boto3 instalados para verificar a presença do dump e se conectar com a AWS.

```sh
docker build -t s3-sender ./app/s3-sender/
```

### Docker Compose
Depois de criar as duas imagens para o container 2 e 3, o arquivo docker-compose.yml iniciado.

```sh
docker-compose up -d
```

Esses três containers funcionarão da seguinte forma:
1. O container 1 inicia e ingere os dados de entrada no MariaDB a partir de um entry-point. O container continua operando.
2. O container 2 inicia, executa o script dump_start.sh que se conecta ao container 1 e realiza o dump em uma pasta compartilhada com o container 3. Logo após este processo, o container termina sua execução e para de operar.
3. O container 3 inicia, inicia o cronjob que executa o script em python para verificar periodicamente a presença do dump na pasta compartilhada com o Container 2 e verifica a existência do bucket para criá-lo e o envia o dump para o bucket do S3. Após esse processo, o dump na pasta compartilhada é excluído e os contêineres continuam verificando a presença de um novo dump. O script realiza o dump do arquivo SQL em uma única pasta por dia, com a nomenclatura respeitando o padrão 'YYYYMMAA' e o nome do arquivo respeitando o padrão 'datadump#.sql', começando com 1, e caso haja mais de um dump no arquivo dia, incrementando o número.
