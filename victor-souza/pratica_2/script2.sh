#!/bin/bash

echo "Criando dois arquivos para armazenar o logs do diretorio e dos processos..."
touch log1.txt log2.txt

echo "Armazenando as informacoes do diretorio no log 1..."
ls -lha > log1.txt

echo "Armazenando as informacoes dos processos no log 2..."
top -n 2 > log2.txt

echo "Criando uma pasta para armazenar os logs..."
mkdir -p backup_logs

echo "Copiando os logs para a pasta..."
cp log1.txt log2.txt ./backup_logs

echo "Atividade concluida com sucesso!"

