#!/bin/bash

echo "Iniciando o script..."

if [[ $1 -gt 2 ]]; then
	echo "Input maior que 2, iniciando o programa..."
	echo "Crindo a pasta de armazenamento..."
	mkdir -p backup_impar backup_par
	echo "Processo finalizado!"
	echo "Iniciando a criacao e copia dos logs..."
	for (( i=1; i<=$1; i++ )); do
		top -n 1 > iteracao-$i.log;

		if (( "$i" % 2 == 0 )); then
			cp iteracao-$i.log ./backup_par
		else
			cp iteracao-$i.log ./backup_impar
		fi
	done
	echo "Processo finalizado!"
	echo "Criando o arquivo em tar..."
	tar -cf backup_par_impar.tar ./backup_par ./backup_impar
	echo "Processo finalizado!"
else
	echo "Input abaixo do esperado, rode novamente com um valor acima de 2."
fi

echo "Script finalizado!"
