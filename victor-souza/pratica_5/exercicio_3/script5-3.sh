#!/bin/bash

echo "Buscando dados do outro exercicio"
cp ../exercicio_2/dados-covid-owid.csv ./

echo "Extraindo os paises do arquivo original"
awk -F "," '{print $3}' dados-covid-owid.csv | sort -u > paises-afetados-covid-owid.txt

echo "Processo finalizado!"
