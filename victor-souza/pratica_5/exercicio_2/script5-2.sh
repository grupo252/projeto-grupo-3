#!/bin/bash

echo "Baixando a base de dados..."
wget -q https://github.com/owid/covid-19-data/archive/refs/heads/master.zip

echo "Unzipando os arquivo baixados..."
unzip -q master.zip

echo "Copiando apenas os arquivos de interesse..."
cp ./covid-19-data-master/public/data/owid-covid-codebook.csv ./manual-dados-covid-owid.csv
cp ./covid-19-data-master/public/data/owid-covid-data.csv ./dados-covid-owid.csv

echo "Removendo os arquivos fora de interesse..."
rm -r ./covid-19-data-master ./master.zip

echo "Processo finalizado!"
