#!/bin/bash


wget https://github.com/owid/covid-19-data/archive/refs/heads/master.zip

# Unzip only the two files we need from master.zip file
unzip master.zip "covid-19-data-master/public/data/owid-covid-data.csv" 
unzip master.zip "covid-19-data-master/public/data/owid-covid-codebook.csv"
rm master.zip

# Create a new directory for the data
mkdir -p dados_covid

# Move the two files to the new directory and overwrite the old files
mv "covid-19-data-master/public/data/owid-covid-data.csv" dados_covid/
mv "covid-19-data-master/public/data/owid-covid-codebook.csv" dados_covid/
rm -r covid-19-data-master


# Rename the files to the correct name
 mv dados_covid/owid-covid-data.csv dados_covid/dados-covid-owid.csv
 mv dados_covid/owid-covid-codebook.csv dados_covid/manual-dados-covid-owid.csv
