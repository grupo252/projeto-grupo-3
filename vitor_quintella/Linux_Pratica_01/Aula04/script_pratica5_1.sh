#!/bin/bash


# Function that returns current date and time as AAA-MM-DD,hh:mm,ss
function current_date(){
  date +"%F,%T"
}
 
# Function that keeps only the 6th and 12th columns of the input file
function keep_columns(){
  awk '{print $12, $6}'
}

# Function that converts first column from KiB to MB
function convert_to_mb(){
  awk '{print $1,",",int($2*0.001024)}'
}

# Function that captures top 5 memory consuming processes
function top5memory(){
  top -b -n 1 -o RES | head -n 12 | tail -n 5
}

# Add current date and time do each line of the input file
function add_date(){
  while read line; do
    echo "$(current_date), $line MB"
  done
}

ROOT_PATH=/home/vitor/gitlab/projeto-grupo-3/vitor_quintella/Linux_Pratica_01/Aula04/memory_logs

# Create directory with YYYY-MM as name if it doesn't exist
if [ ! -d "$ROOT_PATH/$(date +%Y-%m)" ]; then
  mkdir $ROOT_PATH/$(date +%Y-%m)
fi

top5memory | keep_columns | convert_to_mb | add_date >> $ROOT_PATH/$(date +%Y-%m)/$(date +%Y-%m-%d).log



